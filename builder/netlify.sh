#!/bin/sh

NETLIFY_BASE_COMMAND="netlify deploy --site $1 --auth $NETLIFY_TOKEN --dir $2"

if [ "$CI_MERGE_REQUEST_ID" != "" ] || [ "$CI_COMMIT_BRANCH" != "master" ]; then
    echo "Deploy preview"
    $NETLIFY_BASE_COMMAND
elif [ "$CI_COMMIT_BRANCH" == "master" ]; then
    echo "Deploy to production"
    $NETLIFY_BASE_COMMAND --prod
fi
