# LibShare

## Front

Front of this application has moved
[here at libshare/libshare_front](https://gitlab.com/libshare/libshare_front).

## Icon

Current icon is free for non-commercial usage:\
https://pixabay.com/fr/vectors/livre-lecture-livres-apprendre-1157658/

## How to dev

Docker-Compose has been set to ease development. Go to root of libshare project
and run `docker-compose up`, for that you will need docker and docker-compose to
be set on your machine.

Once images are built, you can use and play with the front at
http://localhost:8080/ which is using locally built back-office
http://localhost:5000 and database at domain:`localhost`, with port:`5432`,
user:`root` and password:`toor` on database:`root`.

Documentation is also built on this docker compose, it is then available at
http://localhost:8181.

### Why for development ?

It supports livereload on both HMI and Back-Office. Meaning that modification on
the source code of each application will reload the appropriate server: backend,
frontend or documentation.

This configuration with docker is not made for production. It lacks on security.
Work will be done to ease and document production set up.

### TODO

How to deploy in production ?

mock psycopg2
https://gist.github.com/graphaelli/906b624c18f77f50da5cd0cd4211c3c8
