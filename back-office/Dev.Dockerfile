FROM python:alpine

# Set the working directory to /app
WORKDIR /libshare

# Copy the current directory contents into the container at /app
COPY ./libshare /libshare

RUN apk add postgresql-dev gcc python3-dev musl-dev libffi-dev openssl-dev cargo

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# For dev purpose
ENV FLASK_DEBUG=1
ENV FLASK_ENV=development
ENV FLASK_APP="libshare:create_app('')"

RUN export PYTHONPATH=.
RUN pip install -e .

EXPOSE 5000

CMD flask run --host=0.0.0.0
