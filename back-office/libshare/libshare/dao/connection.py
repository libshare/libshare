"""Database DAO"""
import os
import logging
import psycopg2
import psycopg2.extras
from psycopg2.extensions import connection

logging.basicConfig(level=logging.DEBUG)


def db_connect() -> connection:
    """Connect to database

    Returns
    -------
    connection
        Database connection
    """
    return psycopg2.connect(dbname=os.environ['DB_NAME'],
                            user=os.environ['DB_USER'], password=os.environ['DB_PASSWORD'],
                            host=os.environ['DB_URL'], port=os.environ['DB_PORT'])


def execute_statement(statement: str, data: dict, return_list=False, to_delete=False) -> dict|list[dict]:
    """Execute a database statement

    :param statement: statement to execute
    :type statement: str
    :param data: data associated to statement
    :type data: dict
    :param return_list: True if the statement must return a list, defaults to False
    :type return_list: bool, optional
    :param to_delete: True if the statement must delete an entry, defaults to False
    :type to_delete: bool, optional
    :return: statement return object, generally an entry or a list of entries
    :rtype: dict|list[dict]
    """
    conn = db_connect()
    cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    cur.execute(statement, data)
    conn.commit()
    if return_list:
        return cur.fetchall()
    if to_delete:
        return data
    return cur.fetchone()
