"""Contact DAO"""

import libshare.queries.contact as queries
from libshare.models.contact import Contact
from libshare.dao.connection import execute_statement


def get(contact_info: tuple) -> dict:
    """Get one contact

    :param contact_info: id of contact to retrieve and id of associated user
    :type contact_info: tuple
    :return: found contact
    :rtype: dict
    """
    return execute_statement(queries.GET, {'id': contact_info[0], 'user_id': contact_info[1]})


def get_by_query(info: tuple) -> dict:
    """Get a list of contact based on search query

    :param info: user id and query
    :type info: tuple
    :return: contacts corresponding to query
    :rtype: dict
    """
    return execute_statement(queries.GET_BY_QUERY, {'user_id': info[0], 'query': f'%{info[1]}%'}, return_list=True)


def add(contact: Contact) -> dict:
    """Add a contact

    :param contact: contact to create
    :type contact: Contact
    :return: added contact
    :rtype: dict
    """
    return execute_statement(queries.ADD, vars(contact))


def get_all(user_id: int) -> list[dict]:
    """Get all user contacts

    :param user_id: user id
    :type user_id: int
    :return: a list of contact associated to user
    :rtype: list[dict]
    """
    return execute_statement(queries.GET_ALL, {'user_id': user_id}, return_list=True)


def update(contact: Contact) -> dict:
    """Update contact

    :param contact: contact to update
    :type contact: Contact
    :return: updated contact information
    :rtype: dict
    """
    return execute_statement(queries.UPDATE, vars(contact))


def delete(contact: Contact) -> dict:
    """Remove a contact from Database

    :param contact: contact to delete
    :type contact: Contact
    :return: deleted contact information
    :rtype: dict
    """
    return execute_statement(queries.DELETE, vars(contact), to_delete=True)
