"""Media database queries"""

#: Get a media from its ID and user ID
GET = ("SELECT * "
             "FROM public.media "
             "WHERE id = %(media_id)s AND userid = %(user_id)s;")

#: Get the list of media of a user,
#: using the user ID
GET_ALL = ("SELECT * "
                  "FROM public.media "
                  "WHERE userid = %(user_id)s;")

#: Add a new media
ADD = ("INSERT INTO public.media "
             "(isbn,title,authors,mediaabstract,cover,numberofmedia,shared_id,userid) "
             "VALUES(%(isbn)s,%(title)s,%(authors)s,%(mediaabstract)s,%(cover)s,%(numberofmedia)s,"
             "%(shared_id)s, %(userid)s) "
             "RETURNING *;"
             )

#: Update a media information,
#: using a media ID and its user ID
UPDATE = ("UPDATE public.media "
                "SET isbn=%(isbn)s, title=%(title)s, authors=%(authors)s, "
                "mediaabstract=%(mediaabstract)s, cover=%(cover)s, "
                "numberofmedia=%(numberofmedia)s, shared_id=%(shared_id)s "
                "WHERE id=%(id)s AND userid=%(userid)s "
                "RETURNING *;")

#: Delete a media,
#: using its ID and its user ID
DELETE = ("DELETE FROM public.media "
                "WHERE id=%(id)s AND userid=%(userid)s;")
