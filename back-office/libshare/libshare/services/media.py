"""Media service"""

from typing import Union

import libshare.dao.media as dao
from libshare.models.media import Media
import libshare.services.user as user_service


def get(identity: str, media_id: int = None) -> Union[dict, list[dict]]:
    """Get one or more media of a user

    :param identity: user identity
    :type identity: str
    :param id: id of a media to retrieve, defaults to None
    :type id: int, optional
    :return: one media if its id was sent, else all user medias
    :rtype: Union[dict, list[dict]]
    """
    user = user_service.get(identity)
    if media_id is not None:
        return dao.get_by_id((media_id, user.id))
    return dao.get_all(user.id)


def add(identity: str, data: dict) -> dict:
    """Add a user media

    :param identity: user identity
    :type identity: str
    :param data: media to add
    :type data: dict
    :return: added media
    :rtype: dict
    """
    user = user_service.get(identity)
    data.update({'userid': user.id})
    return dao.add(Media.init_from_dict(data))


def update(identity: str, data: dict) -> dict:
    """Update a media

    :param identity: identity
    :type identity: str
    :param data: media to update
    :type data: dict
    :return: updated media
    :rtype: dict
    """
    user = user_service.get(identity)
    data.update({'userid': user.id})
    return dao.update(Media.init_from_dict(data))


def delete(identity: str, media_id: int) -> dict:
    """Delete one user media

    :param identity: user identity
    :type identity: str
    :param id: id of the media to delete
    :type id: int
    :return: deleted media
    :rtype: dict
    """
    return dao.delete(Media.init_from_dict(get(identity, media_id)))
