"""Contact service"""

import libshare.dao.contact as dao
from libshare.models.contact import Contact
import libshare.services.user as user_service


def get(identity: str, contact_id: int) -> dict:
    """Get one contact from its user and id

    :param identity: user associated with contact to find
    :type identity: str
    :param contact_id: contact unique id
    :type contact_id: int
    :return: contact information
    :rtype: dict
    """
    user = user_service.get(identity)
    return dao.get((contact_id, user.id))


def add(identity: str, data: dict) -> dict:
    """Save a contact

    :param identity: user associated with the contact to save
    :type identity: str
    :param data: contact information
    :type data: dict
    :return: saved contact
    :rtype: dict
    """
    user = user_service.get(identity)
    data.update({'user_id': user.id})
    return dao.add(Contact.init_from_dict(data))


def get_all(identity: str) -> list[dict]:
    """Get user contact list

    :param identity: user identity
    :type identity: str
    :return: a list of contacts
    :rtype: list[dict]
    """
    user = user_service.get(identity)
    return dao.get_all(user.id)


def update(identity: str, data: dict) -> dict:
    """Update contact information

    :param identity: user associated to contact to update
    :type identity: str
    :param data: contact information to update
    :type data: dict
    :return: updated contact information
    :rtype: dict
    """
    user = user_service.get(identity)
    data.update({'user_id': user.id})
    return dao.update(Contact.init_from_dict(data))


def delete(identity: str, contact_id: int) -> dict:
    """Delete a contact

    :param identity: user associated to contact to delete
    :type identity: str
    :param contact_id: unique contact id
    :type contact_id: int
    :return: deleted contact information
    :rtype: dict
    """
    return dao.delete(Contact.init_from_dict(get(identity, contact_id)))

def get_by_query(identity: str, query: str) -> dict:
    """Get a list of contact based on a query

    :param identity: user identity
    :type identity: str
    :param query: query search
    :type query: str
    :return: a list of corresponding contacts
    :rtype: dict
    """
    user = user_service.get(identity)
    return dao.get_by_query((user.id, query))
