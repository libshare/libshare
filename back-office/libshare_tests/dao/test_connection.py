import unittest

import subprocess
import os

from libshare.dao import connection
from libshare.models.user import User


CURRENT_PATH = os.getcwd()


class TestConnection(unittest.TestCase):

    def setUp(self):
        subprocess.run(['sh', '-c', f'{CURRENT_PATH}/libshare_tests/dao/resources/init_db.sh'],
                       timeout=30, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
