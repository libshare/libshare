DROP SCHEMA IF EXISTS public CASCADE;

CREATE SCHEMA public;

CREATE TABLE public.users (
    id SERIAL PRIMARY KEY,
    username VARCHAR (250) NOT NULL UNIQUE,
    email VARCHAR(250) NOT NULL UNIQUE,
    userpassword VARCHAR (250) NOT NULL
);

CREATE TABLE public.media (
    id SERIAL PRIMARY KEY,
    isbn VARCHAR (250),
    title VARCHAR (250) NOT NULL,
    authors VARCHAR (250),
    mediaabstract TEXT,
    cover TEXT,
    numberofmedia INT,
    shared BOOLEAN NOT NULL DEFAULT FALSE,
    userid INTEGER NOT NULL,
    FOREIGN KEY (userid) REFERENCES users(id)
);

INSERT INTO
    public.users (username, email, userpassword)
VALUES
    ('batman', 'batman@batcave.bat', 'robin'),
    ('superman', 'superman@daily.planet', 'lois'),
    ('flash', 'flash@gorilla.grodd', 'iris');

INSERT INTO
    public.media (
        isbn,
        title,
        authors,
        mediaabstract,
        cover,
        numberofmedia,
        shared,
        userid
    )
VALUES
    (
        'AAA',
        'Good book',
        'Myself',
        'Hello world!',
        'url',
        1,
        FALSE,
        1
    ),
    (
        'BBB',
        'Awesome book',
        'Herself',
        'Hola world!',
        'url2',
        1,
        TRUE,
        1
    ),
    (
        'CCC',
        'Amazing book',
        'Himself',
        'Hoy world!',
        'url3',
        1,
        FALSE,
        1
    ),
    (
        'DDD',
        'Spectacular book',
        'Yourself',
        'Hey world!',
        'url4',
        1,
        FALSE,
        2
    );