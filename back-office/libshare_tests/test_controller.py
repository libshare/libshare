import unittest

from unittest.mock import patch
from libshare import create_app
import json
from flask_jwt_extended import JWTManager
from flask_jwt_extended import create_access_token


class TestController(unittest.TestCase):

    EMAIL = 't@t.t'
    PASSWORD = 'titi'
    USERNAME = 'toto'
    MISSING_JSON_MSG = "Missing JSON in request"
    MIME_JSON = 'application/json'
    AUTHENTICATE = '/authenticate'
    REGISTER = '/register'
    headers = {}

    def setUp(self):
        app = create_app('')
        app.config['TESTING'] = True
        app.config['JWT_SECRET_KEY'] = 'test'
        JWTManager(app)
        with app.test_request_context():
            access_token = create_access_token('test')
            self.headers = {'Authorization': f'Bearer {access_token}'}
        self.app = app.test_client()

    def mock_jwt_required(realm):
        return

    # hello()

    def test_hello(self):
        response = self.app.get('/')
        self.assertEqual('text/html', response.mimetype)
        self.assertEqual('Welcome to LibShare API!', response.data.decode())
        self.assertEqual(200, response.status_code)
